import React, { Component } from 'react'
import { Formik, Form, Field } from 'formik';
 import * as Yup from 'yup';

 const FormSchema = Yup.object().shape({
    email: Yup
      .string()
      .email("Email invalid").required("Email can't Emty"),
    pass: Yup.string()
    .matches(/^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,"Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character")
  });

export default class Practice_Validation extends Component {
  render() {
    return (
      <div className=' w-1/2'>
        <Formik
        initialValues={{
          email: '',
          pass: '',
        }}
        validationSchema={FormSchema}
        onSubmit={(values) => {
          console.log(values);
        }}
      >
        {({ errors,touched}) => (
          <Form className='p-5 w-[70%] mx-auto pt-32'>
            <label for="email" class="block mb-2 font-medium text-gray-900 text-left text-xl">Your email</label>
             <Field name="email" type="email" className="shadow appearance-none border rounded w-full py-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"/>
             {errors.email && touched.email ? <div className="text-red-600">{errors.email}</div>:null}
             <label for="email" class="block mb-2 font-medium text-gray-900 text-left text-xl">Password</label>
             <Field name="pass" type="pass" className="shadow appearance-none border rounded w-full py-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"/>
             {errors.pass && touched.pass ? <div className="text-red-500">{errors.pass}</div>:null}
             <button type="submit" class=" my-8 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Login</button>
          </Form>
        )}
      </Formik>
      </div>
    )
  }
}
