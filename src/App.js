import './App.css';
import Practice_Validation from './component/Practice_Validation';

function App() {
  return (
    <div className="App bg-lime-100 min-h-screen">
      <Practice_Validation></Practice_Validation>
    </div>
  );
}

export default App;
